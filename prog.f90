program scavr

    use const_mod
    use fft_mod
    use sc_mod
    implicit none

    real(mp) :: Dt, sigma0, A, X1, aw, Ns
    complex(mp), allocatable, dimension(:) :: X, Dc, c0
    real(mp), allocatable, dimension(:) :: tk, D, nu, c, Wm, Ds
    real(mp), dimension(2) :: ab
    integer :: N, i, k

    Dt = 1.0
    X1 = 9.0
    ab = 0

    print*, "Reading Data"
    call ReadData(X, tk, N,ab)

    call linear(real(X(0:N-1)),tk(0:N-1),N,Dt,ab(1),ab(2))
    ab = linear_fit(X(0:N-1), tk(0:N-1), Dt)
    print*, ab(1), ab(2)
    X(0:N-1) = remove_linear_trend(X(0:N-1), ab, Dt)

    print*, "Writing X(i)"
    open(30,file="nl.dat")
    do i = 0, N-1
        write(30,*) real(X(i))
    enddo
    close(30)

    sigma0 = dot_product(real(X),real(X)) / (N - 1)

    allocate(c(0:size(X, dim=1)-1), Wm(0:size(X, dim=1)-1), c0(0:size(X, dim=1)-1))

    c = 0
    aw = 0.25
    Ns = 0.1 * N

    !$omp parallel do
    do i = 0, N-1
        do k = 0, N-i-1
            c(i) = c(i) + (real(X(k))*real(X(k+i)))
        enddo
        Wm(i) = (1.0-2.0*aw) + 2.0*aw*cos(pi*real(i)/Ns)
    enddo
    !$omp end parallel do

    c = c / N

    open(40, file = "corr.dat")
    do i = 0, N-1
        write(40,*) c(i)
    enddo
    close(40)

    c(Ns:) = 0

    do i = 0, Ns-1
        c(i) = c(i) * Wm(i)
    enddo

    c0 = c*complex(1,0)
    call fft(c0)

    print*, "fft"
    call fft(X)

    allocate(Ds(0:size(X, dim=1)/2), D(0:size(X, dim=1)/2), nu(0:size(X, dim=1)/2))

    Ds(0:) = (2 * real(c0) - c(0)) / Ns


    open(50, file = "c.dat")
    do i = 0, size(c)/2-1
        write(50,*) c(i), real(c0(i)), (2*real(c0(i)) - c(0))
    enddo

    print*, size(X, dim=1)

    print*, "Computing D"
    D = (abs(X(0:size(X, dim=1)/2-1)) / N)**2

    print*, "Computing nu"
    !$omp parallel do
    do i = 0, size(nu, dim=1)-1
        nu(i) = i / real((size(X, dim=1) * Dt))
    enddo 
    !$omp end parallel do

    print*, i

    i = maxloc(D, dim = 1, mask=(D>sigma0*X1/N))

    print*,i

    A = sqrt(4 * D(i) / (W(N,Dt,2*nu(i))+1))

    print*, A, nu(i)

    print*, "Output of periodogramm"
    open(20,file="D.dat")
    do i = 0, size(D, dim=1)-1
        write(20,*) nu(i), D(i), sigma0*X1/N, Ds(i)
    enddo
    close(20)

    deallocate(X,tk,D,nu)
    

contains

    subroutine ReadData(X, tk, N, ab)

        complex(mp), allocatable, dimension(:) :: X!, Dc
        real(mp), allocatable, dimension(:) :: tk, rX
        real(mp), dimension(2)::ab
        real(mp) :: re
        integer :: tmpN, sze, i, N
        character :: s

        open(10, file='series.dat')
        read(10,*) s, N

        tmpN = N
        i = 0

        do while(tmpN .gt. 2)
            tmpN = tmpN / 2
            i = i + 1
        enddo

        if (2**i .lt. N) then
          i = i + 1
          sze = 2**(i)
        else
          sze = N
        endif

        allocate(X(0:2*sze-1), tk(0:sze-1), rX(0:N-1))

        !$omp parallel do
        do i = 0, sze-1
            tk(i) = i * Dt
        enddo
        !$omp end parallel do

        X = cmplx(0,0)
        re = 0

        do i = 0, N-1
          read(10,*) re
          X(i) = cmplx(re,0)
        enddo

        close(10)

        deallocate(rX)

    end subroutine ReadData

end
