program ser_gen
    use const_mod
    use sc_mod
    implicit none

    real(mp) :: Dt, q, X1, A1, nu1, phi1, gamma, alpha, beta, ksi, deltaNu, nuc
    integer :: N, i

    Dt = 1.0
    N = 230
    q = .01
    X1 = 9.0
    A1 = 1.0
    nu1 = .1
    phi1 = 0
    gamma = .5
    alpha = 0.1
    beta = 0.05

    nuc = 0.5 / Dt


    open(10, file = 'series.dat')
    write(10,*) "#", N
    do i = 0, N-1
        call norm_rnd(ksi)
        write(10,*) alpha + beta * Dt * i + A1 * (0*cos(2 * pi * nu1 * Dt * i - phi1) + cos(2 * pi * (nu1 + 2 * 1 * nuc) *&
         Dt * i - phi1)) + ksi * sqrt(A1**2 / (2.0 * gamma))
    enddo
    close(10)

end