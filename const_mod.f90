module const_mod
	integer, parameter  :: mp=8
	real(mp), parameter :: pi=4.0_mp*atan(1.0_mp)
end module const_mod