module sc_mod
    use const_mod
    implicit none
contains

    subroutine norm_rnd(x)

        real(mp) :: s, x
        integer :: j

        x = 0

        do j = 1, 12
            call random_number(s)
            x = x + s
        enddo

        x = x - 6

    end subroutine norm_rnd

    subroutine linear(X,T,N,tk,alpfa,beta)
        implicit none
        real(mp),dimension(0:N-1)::X,T
        integer::N,j
        real(mp)::alpfa, beta,tk

        beta=(dot_product(X,T)*(N)-sum(X)*sum(T))/((N)*dot_product(T,T)-sum(T)**2)
        alpfa=(sum(X)-beta*sum(T))/(N)

        do j=0,N-1
            X(j)=X(j)-alpfa-beta*tk*j
        enddo

    end subroutine linear

    function linear_fit(X, tk, Dt) result(ab)

        integer :: N
        complex(mp), dimension(0:) :: X
        real(mp), dimension(0:) :: tk
        real(mp), dimension(2) :: ab
        real(mp) :: Dt

        N = size(X, dim=1)

        ab(2) = (dot_product(real(X),tk) * N - sum(real(X)) * sum(tk)) / (N * dot_product(tk,tk) - sum(tk)**2)
        ab(1) = (sum(real(X)) - ab(2) * sum(tk)) / N

    end function linear_fit

    function remove_linear_trend(X, ab, Dt) result(Xout)

        complex(mp), dimension(0:) :: X
        real(mp), dimension(0:size(X, dim=1) - 1) :: Xout
        real(mp), dimension(2) :: ab
        real(mp) :: Dt
        integer :: i

        do i = 0, size(X, dim=1)-1
            Xout(i) = real(X(i)) - ab(1) - ab(2) * Dt * i
        enddo

    end function remove_linear_trend

    subroutine PrepareData(x, t)

        complex(mp), dimension(:) :: x
        integer :: t

        if (t .ge. 0) then
           x = conjg(x)
        endif

    end subroutine PrepareData

    function W(N, Dt, nu)

        real(mp) :: W, Dt, nu
        integer :: N

        W = (sin(N * pi * nu * Dt) / (N * sin(pi * nu * Dt)))**2

    end function W

end module sc_mod
